from helpers import prepare_data, visualize_maze, get_maze_name
from UninformedSearch import UninformedSearch
from InformedSearch import InformedSearch
from BonusMazeSearch import BonusMazeSearch


def main():

    maze_name = get_maze_name()
    matrix, start_point, end_point, bonus_points = prepare_data(
        file_name="test/"+maze_name)
    visualize_maze(matrix, bonus_points, start_point, end_point)

    if maze_name not in ["maze_6.txt", "maze_7.txt", "maze_8.txt"]:
        path_dfs, explored_dfs = UninformedSearch.run_dfs(
            matrix, start_point, end_point)
        print(
            f'Chi phí mở rộng DFS: {len(explored_dfs)}, Chi phí đường đi: {len(path_dfs)}')
        if len(path_dfs) > 1:
            visualize_maze(matrix, bonus_points, start_point,
                           end_point, explored_dfs, path_dfs)

        path_bfs, explored_bfs = UninformedSearch.run_bfs(
            matrix, start_point, end_point)
        print(
            f'Chi phí mở rộng BFS: {len(explored_bfs)}, Chi phí đường đi: {len(path_bfs)}')
        if len(path_bfs) > 1:
            visualize_maze(matrix, bonus_points, start_point,
                           end_point, explored_bfs, path_bfs)

        path_a_star, explored_a_star = InformedSearch.run_a_star(
            InformedSearch.euclid_calculation, matrix, start_point, end_point)
        print(
            f'Chi phí mở rộng A*: {len(explored_a_star)}, Chi phí đường đi: {len(path_a_star)}')
        if len(path_a_star) > 1:
            visualize_maze(matrix, bonus_points, start_point,
                           end_point, explored_a_star, path_a_star)

        path_greedy, explored_greedy = InformedSearch.run_greedy(
            InformedSearch.manhattan_calculation, matrix, start_point, end_point)
        print(
            f'Chi phí mở rộng Greedy: {len(explored_greedy)}, Chi phí đường đi: {len(path_greedy)}')
        if len(path_greedy) > 1:
            visualize_maze(matrix, bonus_points, start_point,
                           end_point, explored_greedy, path_greedy)
    else:
        path_bonus, explored_bonus, bonus = BonusMazeSearch.run(
            InformedSearch.euclid_calculation, matrix, start_point, end_point, bonus_points)
        print(
            f'Chi phí mở rộng: {len(explored_bonus)}, Chi phí đường đi: {len(path_bonus)}, Giảm {bonus}, Còn: {len(path_bonus) + bonus}')
        if len(path_bonus) > 1:
            visualize_maze(matrix, bonus_points, start_point,
                           end_point, explored_bonus, path_bonus)
        print('')


if __name__ == '__main__':
    main()
