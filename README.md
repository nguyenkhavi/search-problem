# AI Foundation - Search Problem

| Members       | ID       |
| ------------- | -------- |
| Nguyễn Kha Vĩ | 19120715 |
| Tô Đình Vin   | 19120718 |

# Chạy chương trình:

-   Cập nhật các thư viện cần thiết: `pip3 install -r requirements.txt`
-   Câu lệnh chạy chương trình: `python3 main.py --maze-name <maze-name>`
-   Trong đó `<maze-name>` là: +
    -   maze_1.txt, maze_2.txt, maze_3.txt, maze_4.txt, maze_5.txt: Là các bản đồ không có điểm thưởng
    -   maze_6.txt, maze_7.txt, maze_8.txt Là các bản đồ có điểm thưởng.
        |

# Heuristic

-   1st heuristic algorithm (Euclid): The distance between current point and goal.
-   2nd heuristic algorithm (Manhattan): The value of delta x plus delta y between current point and goal.

## Maze 1 (trường hợp đẹp nhất của DFS):

![Tux, the Linux mascot](/images/Maze1.png)

### Maze 1: Kết quả:

-   A\* Algorithm  
    ![Tux, the Linux mascot](/images/AStarMaze1.png)

-   Greedy Best-First Search Algorithm  
    ![Tux, the Linux mascot](/images/GreedyMaze1.png)

-   Depth-First Search  
    ![Tux, the Linux mascot](/images/DFSMaze1.png)

-   Breath-First Search  
    ![Tux, the Linux mascot](/images/BFSMaze1.png)

### Maze 1: Nhận xét:

-   Cho thấy BFS phải duyệt qua nhiều điểm hơn DFS.
-   DO tính chất của bản đồ, DFS gần như duyệt đúng đường để tìm ra kết quả (may mắn là đường tối ưu).
-   Do heuristic (Euclid) và đặc điểm của bản đồ nên cả GBFS, A\* vẫn phải duyệt qua nhiều điểm trước khi tìm được kết quả tối ưu.

## Maze 2 (trường hợp xấu nhất của DFS):

![Tux, the Linux mascot](/images/Maze2.png)

### Maze 2: Kết quả:

-   Depth-First Search  
    ![Tux, the Linux mascot](/images/DFSMaze2.png)

-   Breath-First Search  
    ![Tux, the Linux mascot](/images/BFSMaze2.png)

-   A\* Algorithm  
    ![Tux, the Linux mascot](/images/AStarMaze2.png)

-   Greedy Best-First Search Algorithm  
    ![Tux, the Linux mascot](/images/GreedyMaze2.png)

### Maze 2: Nhận xét:

-   Cho thấy DFS phải duyệt qua gần như toàn bản đồ để tìm được lối ra.
-   BFS trong trường hợp này phải duyệt qua ít điểm hơn DFS.
-   Do heuristic (Euclid) tương đối hiệu quả đối với đặc điểm của bản đồ nên cả GBFS, A\* hiệu quả hơn so với BFS và DFS.

## Maze 3 (trường hợp DFS không tối ưu):

![Tux, the Linux mascot](/images/Maze3.png)

### Maze 3: Kết quả:

-   Depth-First Search  
    ![Tux, the Linux mascot](/images/DFSMaze3.png)

-   Breath-First Search  
    ![Tux, the Linux mascot](/images/BFSMaze3.png)

-   A\* Algorithm  
    ![Tux, the Linux mascot](/images/AStarMaze3.png)

-   Greedy Best-First Search Algorithm  
    ![Tux, the Linux mascot](/images/GreedyMaze3.png)

### Maze 3: Nhận xét:

-   Cho thấy kết quả của DFS không tối ưu.
-   Kết quả của BFS tối ưu và số điểm phải duyệt ít hơn DFS.
-   Do heuristic (Euclid) vẫn còn hiệu quả đối với đặc điểm của bản đồ nên cả GBFS, A\* hiệu quả hơn so với BFS và DFS.

## Maze 4 (trường hợp DFS và Greedy không tối ưu):

![Tux, the Linux mascot](/images/Maze4.png)

### Maze 4: Kết quả:

-   Depth-First Search  
    ![Tux, the Linux mascot](/images/DFSMaze4.png)

-   Breath-First Search  
    ![Tux, the Linux mascot](/images/BFSMaze4.png)

-   A\* Algorithm  
    ![Tux, the Linux mascot](/images/AStarMaze4.png)

-   Greedy Best-First Search Algorithm  
    ![Tux, the Linux mascot](/images/GreedyMaze4.png)

### Maze 4: Nhận xét:

-   Cho thấy kết quả của DFS không tối ưu. Do thứ tự duyệt có xu hướng đi lên, làm cho tác nhân tìm được đường kh tối ưu đầu tiên.
-   Kết quả của A*, BFS tối ưu. Do hàm heuristic Euclid kém hiệu quả đối với đặc điểm của bản đồ, làm cho thuật toán A* phải duyệt qua nhiều điểm hơn BFS để tìm được lời giải tối ưu.

## Maze 5 (trường hợp Greed không tối ưu khi dùng Manhattan):

Do nhận xét sự thay đổi heuristic nên chỉ nhận xét GBFS và A\*
![Tux, the Linux mascot](/images/Maze5.png)

### Maze 5: Kết quả:

-   A\* Algorithm Manhattan
    ![Tux, the Linux mascot](/images/AStarMaze5Manhattan.png)
-   A\* Algorithm Euclid
    ![Tux, the Linux mascot](/images/AStarMaze5Euclid.png)

-   Greedy Best-First Search Algorithm Manhattan
    ![Tux, the Linux mascot](/images/GreedyMaze5Manhattan.png)
-   Greedy Best-First Search Algorithm Euclid
    ![Tux, the Linux mascot](/images/GreedyMaze5Euclid.png)

### Maze 5: Nhận xét:

-   Kết quả của A\* tối ưu trong 2 trường hợp.
-   Kết quả của Greed không tối ưu và phụ thuộc hoàn toàn vào heuristic (không tối ưu khi dùng Manhattan nhưng tối ưu khi dùng Euclid).
