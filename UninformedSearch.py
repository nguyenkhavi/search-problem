class UninformedSearch:
    @staticmethod
    def run_bfs(matrix=[], start_point=(0, 0), end_point=(0, 0)):
        """
        Perpose:
            - Run Breadth-First Search Algorithm.
        Args:
            - matrix: A matrix created by {0, 1}.
            - start_point: The actor's initial point in the matrix.
            - end_point: The actor's expected point in the matrix.
        Return:
            - The route from initial point (start_point) to expected point (end_point)
        """
        matr = matrix
        frontier = [start_point]
        explored = []
        bfsPath = {}
        while len(frontier) > 0:
            current = frontier.pop(0)
            if current == end_point:
                break
            for d in "NESW":
                if d == 'E':
                    child_point = (current[0], current[1]+1)
                elif d == 'W':
                    child_point = (current[0], current[1]-1)
                elif d == 'N':
                    child_point = (current[0]-1, current[1])
                elif d == 'S':
                    child_point = (current[0]+1, current[1])
                if child_point in explored:
                    continue
                if child_point[0] < 0 or child_point[1] < 0 or child_point[0] > len(matr)-1 or child_point[1] > len(matr[0])-1:
                    continue
                if int(matr[child_point[0]][child_point[1]]) > 0:
                    continue
                frontier.append(child_point)
                if child_point != end_point:
                    explored.append(child_point)
                bfsPath[child_point] = current

        fwdPath = []
        point = end_point
        if bfsPath != {}:
            while point != start_point:
                fwdPath.append(point)

                point = bfsPath[point]

        fwdPath.append(start_point)
        fwdPath.reverse()
        return fwdPath, explored

    @staticmethod
    def run_dfs(matrix=[], start_point=(0, 0), end_point=(0, 0)):
        """
        Perpose:
            - Run Depth-First Search Algorithm.
        Args:
            - matrix: A matrix created by {0, 1}.
            - start_point: The actor's initial point in the matrix.
            - end_point: The actor's expected point in the matrix.
        Return:
            - The route from initial point (start_point) to expected point (end_point)
        """
        matr = matrix
        frontier = [start_point]
        explored = [start_point]
        dfsPath = {}
        while len(frontier) > 0:
            current = frontier.pop(len(frontier) - 1)
            if current == end_point:
                break
            for d in "NESW":
                if d == 'E':
                    child_point = (current[0], current[1]+1)
                elif d == 'W':
                    child_point = (current[0], current[1]-1)
                elif d == 'N':
                    child_point = (current[0]-1, current[1])
                elif d == 'S':
                    child_point = (current[0]+1, current[1])
                if child_point in explored:
                    continue
                if child_point[0] < 0 or child_point[1] < 0 or child_point[0] > len(matr)-1 or child_point[1] > len(matr[0])-1:
                    continue
                if int(matr[child_point[0]][child_point[1]]) > 0:
                    continue
                frontier.append(child_point)
                if child_point != end_point:
                    explored.append(child_point)
                dfsPath[child_point] = current

        fwdPath = []
        point = end_point
        if dfsPath != {}:
            while point != start_point:
                fwdPath.append(point)

                point = dfsPath[point]

        fwdPath.append(start_point)
        fwdPath.reverse()
        return fwdPath, explored
