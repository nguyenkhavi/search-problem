import matplotlib.pyplot as plt
from argparse import ArgumentParser


def get_maze_name():
    parser = ArgumentParser()
    parser.add_argument('--maze-name')
    args = parser.parse_args()
    return args.maze_name


def is_function(function):
    def sample_function():
        pass
    return type(function) == type(sample_function)


def prepare_data(file_name: str = 'maze.txt'):
    """
    Perpose:
        - Read file and repare data
    Args:
        - file_name: Read data from 'file_name'.
    Return:
        - matrix: A matrix created by {0, 1}.
        - start_point: The actor's initial point in the matrix.
        - end_point: The actor's expected point in the matrix.    
    """
    f = open(file_name, 'r')

    n_bonus_points = int(next(f)[:-1])
    bonus_points = []
    for i in range(n_bonus_points):
        x, y, reward = map(int, next(f)[:-1].split(' '))
        bonus_points.append((x, y, reward))

    text = f.read()
    matrix = []
    start_point = 0, 0
    end_point = 0, 0

    lines = text.splitlines()
    for i in range(len(lines)):
        line = list(lines[i].replace('x', '1').replace(
            ' ', '0').replace('+', '0'))
        for j in range(len(line)):
            if line[j] == 'S':
                line[j] = 0
                start_point = i, j
            if i == 0 or i == len(lines) - 1 or j == 0 or j == len(line) - 1:
                if int(line[j]) == 0:
                    end_point = i, j

        matrix.append([int(bit) for bit in line])
    f.close()
    return matrix, start_point, end_point, bonus_points


def visualize_maze(matrix, bonus, start, end, explored=None, route=None):
    """
    Args:
      1. matrix: The matrix read from the input file,
      2. bonus: The array of bonus points,
      3. start, end: The starting and ending points,
      4. route: The route from the starting point to the ending one, defined by an array of (x, y), e.g. route = [(1, 2), (1, 3), (1, 4)]
    """
    # 1. Define walls and array of direction based on the route
    walls = [(i, j) for i in range(len(matrix))
             for j in range(len(matrix[0])) if matrix[i][j] == 1]
    if route:
        direction = []
        for i in range(1, len(route)):
            if route[i][0]-route[i-1][0] > 0:
                direction.append('v')  # ^
            elif route[i][0]-route[i-1][0] < 0:
                direction.append('^')  # v
            elif route[i][1]-route[i-1][1] > 0:
                direction.append('>')
            else:
                direction.append('<')

        direction.pop(0)

    # 2. Drawing the map
    ax = plt.figure(dpi=100).add_subplot(111)

    for i in ['top', 'bottom', 'right', 'left']:
        ax.spines[i].set_visible(False)

    if explored:
        for i in range(len(explored)-2):
            plt.scatter(explored[i+1][1], -explored[i+1][0],
                        marker='.', color='red')

    plt.scatter([i[1] for i in walls], [-i[0] for i in walls],
                marker='X', s=100, color='black')

    plt.scatter([i[1] for i in bonus], [-i[0] for i in bonus],
                marker='P', s=100, color='green')

    plt.scatter(start[1], -start[0], marker='*',
                s=100, color='gold')

    if route:
        for i in range(len(route)-2):
            plt.scatter(route[i+1][1], -route[i+1][0],
                        marker=direction[i], color='silver')

    plt.text(end[1], -end[0], 'EXIT', color='red',
             horizontalalignment='center',
             verticalalignment='center')
    plt.xticks([])
    plt.yticks([])
    plt.show()

    # print(f'Starting point (x, y) = {start[0], start[1]}')
    # print(f'Ending point (x, y) = {end[0], end[1]}')

    # for _, point in enumerate(bonus):
    #     print(
    #         f'Bonus point at position (x, y) = {point[0], point[1]} with point {point[2]}')
