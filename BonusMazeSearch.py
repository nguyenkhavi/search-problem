from InformedSearch import InformedSearch


class BonusMazeSearch:
    @staticmethod
    def get_closest_point(point=(0, 0), bonus_points=[]):
        if len(bonus_points) == 0:
            return -1

        minDistance = InformedSearch.euclid_calculation(
            point, (bonus_points[0][0], bonus_points[0][1]))
        temp = bonus_points[0]
        for p in bonus_points:
            tempValue = InformedSearch().euclid_calculation(
                point, (p[0], p[1]))
            if tempValue < minDistance:
                minDistance = tempValue
                temp = p

        return (temp[0], temp[1]), temp[2]

    @staticmethod
    def run(heuristic_function, matrix=[], start_point=(0, 0), end_point=(0, 0), bonus_points=[]):
        try:
            if start_point == end_point:
                return ([], [], 0)

            run_to_end = InformedSearch.run_greedy(
                heuristic_function, matrix, start_point, end_point)

            if len(bonus_points) == 0:
                return run_to_end + (0, )

            closet_bonus, closet_bonus_value = BonusMazeSearch.get_closest_point(
                start_point, bonus_points)
            len_to_end = heuristic_function(start_point, end_point)

            len_to_closest_bonus = heuristic_function(
                start_point, closet_bonus)

            run = BonusMazeSearch.run(heuristic_function, matrix, closet_bonus, end_point, bonus_points=[
                                      i for i in bonus_points if (i[0], i[1]) != closet_bonus])
            run_greedy_to_closest_bonus = InformedSearch.run_greedy(
                heuristic_function, matrix, start_point, closet_bonus)
            if len_to_end >= len_to_closest_bonus and abs(closet_bonus_value) > len_to_closest_bonus:
                print('Go to bonus')
                print(closet_bonus)
                return list(run[0]) + list(run_greedy_to_closest_bonus[0]), list(run[1]) + list(run_greedy_to_closest_bonus[1]), closet_bonus_value + run[2]
            else:
                return run_to_end + (0, )
        except:
            raise Exception(
                f'[INFO] Get stuck when trying to collect bonus point!!!')
