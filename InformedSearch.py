from queue import PriorityQueue
from helpers import is_function
from numpy import sqrt


class InformedSearch:

    @staticmethod
    def euclid_calculation(point1, point2):
        """A Heuristic Function.
        Perpose:
            - Calculate the distance between 2 2D points.
        Args:
            - point1: A point in matrix.
            - point2: A point in matrix.
        Return:
            - Return the value of (x1 - x2)^2 + (y1 - y2)^2
        """
        x1, y1 = point1
        x2, y2 = point2
        return sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2))

    @staticmethod
    def manhattan_calculation(point1, point2):
        """A Heuristic Function.
        Perpose:
            - Calculate mahattan between 2 2D points.
        Args:
            - point1: A point in matrix.
            - point2: A point in matrix.
        Return:
            - Return the value of abs(x1 - x2) + abs(y1 - y2)
        """
        x1, y1 = point1
        x2, y2 = point2
        return abs(x1-x2)+abs(y1-y2)

    @staticmethod
    def run_a_star(heuristic_function, matrix=[], start_point=(0, 0), end_point=(0, 0)):
        """A Function.
        Perpose:
            - Run A* Search Algorithm.
        Args:
            - heuristic_function: a heuristic function
            - matrix: A matrix created by {0, 1}.
            - start_point: The actor's initial point in the matrix.
            - end_point: The actor's expected point in the matrix.
        Return:
            - The route from initial point (start_point) to expected point (end_point)
        """
        if not is_function(heuristic_function):
            raise ValueError('Must have a heuristic function passed!!!')

        mx = matrix
        start = start_point
        end = end_point
        aPath = {}
        g_score = {
            (i, j): float('inf') for i in range(len(mx))
            for j in range(len(mx[0]))
            if mx[i][j] == 0

        }
        g_score[start] = 0
        f_score = {
            (i, j): float('inf') for i in range(len(mx))
            for j in range(len(mx[0]))
            if mx[i][j] == 0

        }
        f_score[start] = heuristic_function(start, end)

        open = PriorityQueue()
        open.put((heuristic_function(start, end),
                 heuristic_function(start, end), start))
        explored = []
        while not open.empty():
            current = open.get()[2]
            if current == end_point:
                break
            for d in "NESW":
                if d == 'E':
                    child_point = (current[0], current[1]+1)
                elif d == 'W':
                    child_point = (current[0], current[1]-1)
                elif d == 'N':
                    child_point = (current[0]-1, current[1])
                elif d == 'S':
                    child_point = (current[0]+1, current[1])

                if child_point[0] < 0 or child_point[1] < 0 or child_point[0] > len(mx)-1 or child_point[1] > len(mx[0])-1:
                    continue
                if int(mx[child_point[0]][child_point[1]]) > 0:
                    continue

                temp_g_score = g_score[current]+1
                temp_f_score = temp_g_score + \
                    heuristic_function(child_point, end)

                if(child_point != end_point):
                    explored.append(child_point)
                if temp_f_score < f_score[child_point]:
                    g_score[child_point] = temp_g_score
                    f_score[child_point] = temp_f_score
                    open.put((temp_f_score, heuristic_function(
                        child_point, end), child_point))
                    aPath[child_point] = current

        fwdPath = []
        point = end_point
        while point != start_point:
            fwdPath.append(point)
            point = aPath[point]

        fwdPath.append(start_point)
        fwdPath.reverse()
        return fwdPath, explored

    @staticmethod
    def run_greedy(heuristic_function, matrix=[], start_point=(0, 0), end_point=(0, 0)):
        """A Function.
        Perpose:
            - Run Greedy Best-First Search Algorithm.
        Args:
            - heuristic_function: a heuristic function
            - matrix: A matrix created by {0, 1}.
            - start_point: The actor's initial point in the matrix.
            - end_point: The actor's expected point in the matrix.
        Return:
            - The route from initial point (start_point) to expected point (end_point)
        """
        if not is_function(heuristic_function):
            raise ValueError('Must have a heuristic function passed!!!')
        mx = matrix
        start = start_point
        end = end_point
        greedyPath = {}

        f_score = {
            (i, j): float('inf') for i in range(len(mx))
            for j in range(len(mx[0]))
            if mx[i][j] == 0

        }
        f_score[start] = heuristic_function(start, end)

        explored = []
        open = PriorityQueue()
        open.put((heuristic_function(start, end), start))
        while not open.empty():

            current = open.get()[1]

            if current == end_point:
                break
            for d in "NESW":
                if d == 'E':
                    child_point = (current[0], current[1]+1)
                elif d == 'W':
                    child_point = (current[0], current[1]-1)
                elif d == 'N':
                    child_point = (current[0]-1, current[1])
                elif d == 'S':
                    child_point = (current[0]+1, current[1])

                if child_point[0] < 0 or child_point[1] < 0 or child_point[0] > len(mx)-1 or child_point[1] > len(mx[0])-1:
                    continue
                if int(mx[child_point[0]][child_point[1]]) > 0:
                    continue
                temp_f_score = heuristic_function(child_point, end)
                if(child_point != end_point):
                    explored.append(child_point)
                if temp_f_score < f_score[child_point]:
                    f_score[child_point] = temp_f_score
                    open.put((temp_f_score, child_point))
                    greedyPath[child_point] = current

        fwdPath = []
        point = end_point
        while point != start_point:
            fwdPath.append(point)
            point = greedyPath[point]
        fwdPath.append(start_point)
        fwdPath.reverse()

        return fwdPath, explored
